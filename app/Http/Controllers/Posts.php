<?php
/**
 * Created by PhpStorm.
 * User: Corals
 * Date: 11/30/2018
 * Time: 10:48 PM
 */

namespace App\Http\Controllers;

use App\Category;
use App\Post;

class Posts
{
    public function getStatusOptions()
    {
        return [
            'active',
            'inactive'
        ];
    }

    public function getPosts($request)
    {
        $posts = Post::query();

        foreach ($request->all() as $filter => $value) {
            $filterMethod = $filter . 'QueryBuilderFilter';

            if (method_exists($this, $filterMethod) && !empty($value)) {
                $posts = $this->{$filterMethod}($posts, $value);

            }
        }

        $posts = $posts->addSelect('*')->get();

        return $posts;
    }


    protected function searchQueryBuilderFilter($posts, $value)
    {
        $posts = $posts->where('title', 'like', '%' . $value . '%');
        return $posts;
}

    protected function categoryQueryBuilderFilter($posts, $category)
    {
        $posts = $posts
            ->join('category_post', 'posts.id', '=', 'category_post.post_id')
            ->join('categories','categories.id', '=' ,'category_post.category_id')
            ->where('status' , '=' ,'active')
            ->whereIn('category_post.category_id', $category);
//        dd($this->getQueryWithParameters($posts));
        return $posts;
    }


//    protected function dateQueryBuilderFilter($posts, $date)
//    {
//        $posts = $posts
//            ->where('status','active')
//            ->whereBetween('created_at', [$date['start'], $date['end']]);
//        dd($this->getQueryWithParameters($posts));
//        return $posts;
//    }











    protected function getQueryWithParameters($query)
    {
        $sql = $query->toSql();
        $parameters = $query->getBindings();
        $result = "";

        $sql_chunks = explode('?', $sql);
        foreach ($sql_chunks as $key => $sql_chunk) {
            if (isset($parameters[$key])) {
                $result .= $sql_chunk . '"' . $parameters[$key] . '"';
            }
        }
        return $result;
    }


}