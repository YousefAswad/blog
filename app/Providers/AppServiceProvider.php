<?php

namespace App\Providers;

use App\Http\Controllers\Posts;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\App;
use Illuminate\Foundation\AliasLoader;
use App\Facades\PostFacade;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        App::bind('PostFacade', function () {
            return new Posts();
        });
        App::booted(function () {
            $loader = AliasLoader::getInstance();
            $loader->alias('Posts', PostFacade::class);
        }
        );
    }
}
