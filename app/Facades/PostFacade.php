<?php
/**
 * Created by PhpStorm.
 * User: Corals
 * Date: 11/30/2018
 * Time: 10:40 PM
 */

namespace App\Facades;


use Illuminate\Support\Facades\Facade;

class PostFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'PostFacade';
    }

}