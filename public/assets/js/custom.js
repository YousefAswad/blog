// conflict bugg solution 
var $ = jQuery.noConflict();


$(function () {

// intialize WOW
    new WOW().init();

    $(document).click(function (event) {
        var clickover = $(event.target);
        var _opened = $(".navbar-collapse").hasClass("navbar-collapse collapse show");
        console.log(_opened);
        if (_opened === true && !clickover.hasClass("navbar-toggler")) {
            $(".navbar-toggler").click();
        }
    });


    $("#bot-to-top").click(function () {
        $('html, body').animate({
            scrollTop: $('body').offset().top
        });
    });

});

$(window).scroll(function () {
    if ($(this).scrollTop() > 300)
    {
        $('#bot-to-top').show(1000);
    } else
    {
        $('#bot-to-top').hide(1000);
    }
});

$(window).on('load', function () {

//    Initialize pre-loader
    $('.startLoad').fadeOut('slow');
});