@extends('layouts.public')
@section('content')
    {!! Form::model($category,['url'=>'categories/'.$category->id,'method' => 'PUT']) !!}
    <div class="form-group">
        {!! Form::label('Name','The Name is :') !!}
        {!! Form::text('name' ,null, ['class' => 'form-control']) !!}

    </div>

    {!! Form::submit('Create',['class'=>'btn btn-primary']) !!}
    {!! Form::close() !!}
@stop