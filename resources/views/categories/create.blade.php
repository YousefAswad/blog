@extends('layouts.public')
@section('content')
    {!! Form::open(['url'=>'categories','METHOD' => 'POST']) !!}
    <div class="form-group">
        {!! Form::label('Name','The Name is :') !!}
        {!! Form::text('name' ,null, ['class' => 'form-control']) !!}

    </div>

    {!! Form::submit('Create',['class'=>'btn btn-primary']) !!}
    {!! Form::close() !!}
@stop