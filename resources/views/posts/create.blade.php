@extends('layouts.public')
@section('content')
    {!! Form::open(['url'=>'posts','METHOD' => 'POST', 'files' => true]) !!}
    <div class="form-group">
        {!! Form::label('Titlte','The Title is :') !!}
        {!! Form::text('title' ,null, ['class' => 'form-control']) !!}

    </div>
    <div class="form-group">
        {!! Form::label('Body','The Body is :') !!}
        {!! Form::textarea('body',null,['class'=>'form-control' ,'rows'=>'8']) !!}
    </div>
    <div class="form-group">
        {!!Form::select('categories[]', $categories , null, ['class' => 'form-control' ,'multiple'=>'multiple'])!!}
    </div>

    <div class="form-group">
        {!! Form::label('Status','The status is :') !!}<br>
        @foreach(\Posts::getStatusOptions() as $key => $value)
            {!! Form::label($value) !!}
            {!! Form::radio('status',$value) !!}
        @endforeach

    </div>

    <div class="form-group">
        {!! Form::label('Image','Image :') !!}<br>
        {!! Form::file('image',['class' => 'form-control' , 'multiple' => 'multiple']) !!}
    </div>

    {!! Form::submit('Create',['class'=>'btn btn-primary']) !!}
    {!! Form::close() !!}
@stop