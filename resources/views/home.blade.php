<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Blog Post - Start Bootstrap Template</title>

    <!-- Bootstrap core CSS -->
    <link href="css/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/css/blog-post.css" rel="stylesheet">

</head>

<body>

<!-- Navigation -->
<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
    <div class="container">
        <a class="navbar-brand" href="{{route('posts.create')}}">Start Bootstrap</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive"
                aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item active">
                    <a class="nav-link" href="#">Home
                        <span class="sr-only">(current)</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">About</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Services</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Contact</a>
                </li>
            </ul>
        </div>
    </div>
</nav>

<!-- Page Content -->
<div class="container">
    <div class="row">
        <!-- Post Content Column -->
        <div class="col-lg-8">
            <!-- Title -->
            <h1 class="mt-4">Post Title</h1>
            <!-- Author -->
            <p class="lead">
                <a class="btn btn-primary" href="{{route('posts.create')}}">Create Post !!</a>
            </p>
            <p class="lead">
                <a class="btn btn-info" href="{{route('categories.create')}}">Create Category !!</a>
            </p>
            <hr>
            @foreach($posts as $post)

                <p><strong>{{ $post->title }}</strong></p>
                <hr>
                <!-- Preview Image -->
                @foreach($post->getMedia('image') as $item)
                    <img class="img-fluid rounded" src="{{ asset($item->getUrl())  }}" alt="">
                @endforeach
                <hr>
                <p>
                    {{ $post->body }}
                </p>
                @foreach($post->categories as $category)
                    <a class="btn btn-success my-2 my-sm-0"
                       href="{{ url('categories/'.$category->id) }}">{{ $category->name }}</a>
                @endforeach
                <div class="mt-5">
                    <a href="{{url('posts/'.$post->id.'/edit')}}" class="btn btn-primary">Edit</a>
                </div>
                <hr>
            @endforeach
        </div>
        <!-- Sidebar Widgets Column -->
        <div class="col-md-4">
            <!-- Search Widget -->
            <div class="card my-4">
                <form>
                    {{csrf_field()}}
                    <h5 class="card-header">Search</h5>
                    <div class="card-body">
                        <div class="input-group">
                            <input type="text" value="{{request()->get('search')}}" name="search" class="form-control"
                                   placeholder="Search for...">
                            <span class="input-group-btn">
                  <button class="btn btn-secondary" type="submit">Go!</button>
                </span>

                        </div>
                    </div>
                    <h5 class="card-header">Search Category</h5>
                    <div class="card-body">
                        <div class="input-group" style="display: table-footer-group;">
                            @foreach($categories as $category)
                                <input type="checkbox" name="category[]" value="{{$category->id}}" />{{$category->name}}
                            @endforeach
                            <span class="input-group-btn">
                  <button class="btn btn-secondary" type="submit">Go!</button>
                </span>

                        </div>
                    </div>
                    <h5 class="card-header">Search Date</h5>
                    <div class="card-body">
                        <div class="input-group">
                            {{--<input class="form-control" type="date" name="date[start]"  />--}}
                            {{--<input class="form-control" type="date" name="date[end]" />--}}
                            <span class="input-group-btn">
                  <button class="btn btn-secondary" type="submit">Go!</button>
                </span>

                        </div>
                    </div>
                </form>
            </div>
            <!-- Categories Widget -->
            <div class="card my-4">
                <h5 class="card-header">Categories</h5>
                <div class="card-body">
                    <div class="row">
                        <div class="col-lg-6">
                            <ul class="list-unstyled mb-0">
                                @foreach($categories as $category)
                                    <li>
                                        <a href="{{ url('categories/'.$category->id) }}"
                                           style="margin-right: 20px">{{ $category->name }}</a>
                                        <a class="btn btn-primary"
                                           href="{{ url('categories/'.$category->id.'/edit') }}">Edit</a>
                                        <hr>
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /.row -->
</div>
<!-- /.container -->
<!-- Footer -->
<footer class="py-5 bg-dark">
    <div class="container">
        <p class="m-0 text-center text-white">Copyright &copy; Your Website 2018</p>
    </div>
    <!-- /.container -->
</footer>
<!-- Bootstrap core JavaScript -->
<script src="css/vendor/jquery/jquery.min.js"></script>
<script src="css/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>




</body>

</html>
